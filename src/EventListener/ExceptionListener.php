<?php

namespace App\EventListener;

use App\Exception\ValidSerializable\ValidatorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {

        $throwable = $event->getThrowable();

        $data = [
            'code' => $throwable->getCode(),
            'message' => $throwable->getMessage(),
        ];

        if ($throwable instanceof ValidatorException) {
            foreach ($throwable->getValidationResult() as $error) {
                $data['errors'][] = [
                    'property' =>  (string) $error->getPropertyPath(),
                    'error' =>  (string) $error->getMessage(),
                ];
            }
        }

        $event->setResponse(new JsonResponse($data, 500));
    }
}