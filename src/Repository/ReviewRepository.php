<?php

namespace App\Repository;

use App\Entity\Review;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Review>
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /**
     * Метод для сохранения отзыва
     */
    public function save(Review $review, bool $flush = false): void
    {
        $this->getEntityManager()->persist($review);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Метод для удаления отзыва
     */
    public function remove(Review $review, bool $flush = false): void
    {
        $this->getEntityManager()->remove($review);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Метод для поиска отзывов по курсу
     */
    public function findByCourse(int $courseId): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.course = :courseId')
            ->setParameter('courseId', $courseId)
            ->orderBy('r.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
