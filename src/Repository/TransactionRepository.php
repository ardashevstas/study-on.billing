<?php

namespace App\Repository;

use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends ServiceEntityRepository<Transaction>
 *
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function save(Transaction $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Transaction $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByUserAndFilters(UserInterface $user, ?array $filter)
    {
        $qb = $this->createQueryBuilder('t');

        $qb->where('t.user = :user')
            ->setParameter('user', $user);

        if (isset($filter['type'])) {
            $qb->andWhere('t.operationType = :type')
                ->setParameter('type', $filter['type']);
        }

        if (isset($filter['course_code'])) {
            $qb->leftJoin('t.course', 'c')
                ->andWhere('c.code = :code')
                ->setParameter('code', $filter['course_code']);
        }

        if (isset($filter['skip_expired']) && $filter['skip_expired']) {
            $qb->andWhere('t.expirationDateTime > :now')
                ->setParameter('now', new \DateTime());
        }

        return $qb->getQuery()->getResult();
    }
}
