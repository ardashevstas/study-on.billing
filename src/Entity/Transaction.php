<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: "App\Repository\TransactionRepository")]
class Transaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\ManyToOne(targetEntity: Course::class)]
    private $course;

    #[ORM\Column(type: "smallint")]
    private $operationType;

    #[ORM\Column(type: "float")]
    private $value;

    #[ORM\Column(type: "datetime")]
    private $transactionDateTime;

    #[ORM\Column(type: "datetime", nullable: true)]
    private $expirationDateTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getOperationType(): ?int
    {
        return $this->operationType;
    }

    public function setOperationType(int $operationType): self
    {
        $this->operationType = $operationType;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getTransactionDateTime(): ?DateTimeInterface
    {
        return $this->transactionDateTime;
    }

    public function setTransactionDateTime(DateTimeInterface $transactionDateTime): self
    {
        $this->transactionDateTime = $transactionDateTime;

        return $this;
    }

    public function getExpirationDateTime(): ?DateTimeInterface
    {
        return $this->expirationDateTime;
    }

    public function setExpirationDateTime(?DateTimeInterface $expirationDateTime): self
    {
        $this->expirationDateTime = $expirationDateTime;

        return $this;
    }
}
