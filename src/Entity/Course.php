<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: "App\Repository\CourseRepository")]
#[UniqueEntity("code", message: "A course with this code already exists.")]
#[UniqueEntity("title", message: "A course with this title already exists.")]
class Course
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private ?int $id = null;

    #[ORM\Column(type: "string", length: 255, unique: true)]
    #[Assert\NotBlank(message: "Code should not be blank.")]
    private ?string $code = null;

    #[ORM\Column(type: "string", length: 255, unique: true)]
    private ?string $title = null;

    #[ORM\Column(type: "smallint")]
    #[Assert\Choice(choices: [1, 2, 3], message: "Choose a valid type.")]
    private ?int $type = null;

    #[ORM\Column(type: "float")]
    #[Assert\NotBlank(message: "Price should not be blank.")]
    #[Assert\GreaterThanOrEqual(value: 0, message: "The price should be 0 or greater.")]
    private ?float $cost = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCost(): ?float
    {
        return $this->cost;
    }

    public function setCost(float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }
}
