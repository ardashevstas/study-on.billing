<?php

namespace App\Dto;

class CourseDTO
{
    public function __construct(
        private readonly string $code,
        private readonly string $type,
        private readonly float $price
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPrice(): float
    {
        return $this->price;
    }
}
