<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;
class TopupDTO
{
    #[Assert\NotBlank(message: "Amount is required.")]
    #[Assert\Type("float")]
    #[Assert\Positive(message: "Amount must be greater than zero.")]
    public float $amount;
}