<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UserRegisterDTO
{
    #[Assert\NotBlank(message: "Email is mandatory")]
    #[Assert\Email(message: "Invalid email address")]
    public string $email;

    #[Assert\NotBlank(message: "Password is mandatory")]
    #[Assert\Length(min: 6, minMessage: "Password should be at least 6 characters")]
    public string $password;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
}