<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CourseRequestDTO
{
    #[Assert\NotBlank(message: "Type is required.")]
    #[Assert\Choice(choices: ['1', '2', '3'], message: "Invalid type value.")]
    public string $type;

    #[Assert\NotBlank(message: "Title is required.")]
    #[Assert\Type("string")]
    public string $title;

    #[Assert\NotBlank(message: "Code is required.")]
    #[Assert\Type("string")]
    public string $code;

    #[Assert\NotBlank(message: "Price is required.")]
    #[Assert\Type("float")]
    public float $price;
}
