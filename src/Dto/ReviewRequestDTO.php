<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ReviewRequestDTO
{
    #[Assert\NotBlank(message: "Rating is required.")]
    #[Assert\Range(notInRangeMessage: "Rating should be between 1 and 5.", min: 1, max: 5)]
    public int $rating;

    #[Assert\NotBlank(message: "Comment is required.")]
    #[Assert\Length(max: 500, maxMessage: "Comment cannot be longer than 500 characters.")]
    public string $comment;
}
