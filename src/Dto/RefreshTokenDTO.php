<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class RefreshTokenDTO
{
    #[Assert\NotBlank(message: "Refresh token is required.")]
    #[Assert\Type("string")]
    public string $refresh_token;
}