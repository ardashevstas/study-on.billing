<?php

namespace App\Resolver;

use App\Attribute\ValidSerializable;
use App\Exception\ValidSerializable\SerializerResolverException;
use App\Exception\ValidSerializable\ValidatorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidSerializableArgumentResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly ValidatorInterface $validator
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return (bool) $argument->getAttributes(ValidSerializable::class, ArgumentMetadata::IS_INSTANCEOF);
    }

    /**
     * @throws ValidatorException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        try {
            $data = $this->serializer->deserialize(
                $request->getContent(),
                $argument->getType(),
                'json',
            );
        } catch (\Exception $exception) {
            throw new SerializerResolverException(
                sprintf('Ошибка при сериализации аргумента: %s', $exception->getMessage()),
                400,
                $exception
            );
        }

        $validationResult = $this->validator->validate($data);

        if ($validationResult->count() > 0) {
            throw new ValidatorException(
                validationResult: $validationResult,
                message: 'Не валидные данные',
                code: 400,
            );
        }

        yield $data;
    }
}