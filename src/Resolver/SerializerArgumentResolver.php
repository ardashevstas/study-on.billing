<?php

namespace App\Resolver;

use App\Attribute\Serializable;
use App\Exception\ValidSerializable\SerializerResolverException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class SerializerArgumentResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return (bool)$argument->getAttributes(Serializable::class, ArgumentMetadata::IS_INSTANCEOF);
    }

    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        /**
         * @var Serializable $attribute
         */
        $attribute = $argument->getAttributes(Serializable::class, ArgumentMetadata::IS_INSTANCEOF)[0];

        $context = [];
        if ($attribute->getGroups()) {
            $context['groups'] = $attribute->getGroups();
        }

        try {
            yield $this->serializer->deserialize(
                $request->getContent(),
                $argument->getType(),
                'json',
                $context
            );
        } catch (Throwable $exception) {
            throw new SerializerResolverException(
                sprintf('Ошибка при сериализации аргумента: %s', $exception->getMessage()),
                500,
                $exception
            );
        }

    }
}