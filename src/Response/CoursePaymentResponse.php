<?php

namespace App\Response;

use App\Response\SuccessResponse;

class CoursePaymentResponse extends SuccessResponse
{
    public function __construct(
        private readonly string $courseType,
        private readonly string $expiresAt
    ) {
    }

    public function getCourseType(): string
    {
        return $this->courseType;
    }

    public function getExpiresAt(): string
    {
        return $this->expiresAt;
    }
}