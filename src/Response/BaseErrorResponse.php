<?php

namespace App\Response;

class BaseErrorResponse extends BaseResponse
{
    public function __construct(string $message)
    {
        parent::__construct(false, $message);
    }
}