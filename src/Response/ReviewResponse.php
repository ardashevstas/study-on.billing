<?php

namespace App\Response;

use App\Response\SuccessResponse;

class ReviewResponse extends SuccessResponse
{
    public function __construct(
        private readonly int $id,
        private readonly int $userId,
        private readonly string $userEmail,
        private readonly int $courseId,
        private readonly string $courseTitle,
        private readonly int $rating,
        private readonly string $comment,
        private readonly \DateTimeInterface $createdAt
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @return int
     */
    public function getCourseId(): int
    {
        return $this->courseId;
    }

    /**
     * @return string
     */
    public function getCourseTitle(): string
    {
        return $this->courseTitle;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
