<?php

namespace App\Response;

class RegisterResponse extends SuccessResponse
{
    public function __construct(
        private readonly string $token,
        private readonly string $refreshToken
    ) {
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }
}
