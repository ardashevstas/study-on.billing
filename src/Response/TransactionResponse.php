<?php

namespace App\Response;

use App\Response\SuccessResponse;

class TransactionResponse extends SuccessResponse
{
    public function __construct(
        private readonly int $id,
        private readonly string $createdAt,
        public ?string $expiration,
        private readonly string $type,
        private readonly float $amount,
        private readonly ?string $courseCode,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getExpiration(): ?string
    {
        return $this->expiration;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCourseCode(): ?string
    {
        return $this->courseCode;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }
}