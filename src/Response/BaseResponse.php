<?php

namespace App\Response;

class BaseResponse
{
    protected bool $success;
    protected ?string $message;

    /**
     * @param bool $success
     * @param null|string $message
     */
    public function __construct(bool $success, ?string $message)
    {
        $this->success = $success;
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}