<?php

namespace App\Response;

class CoursesResponse extends SuccessResponse
{
    public function __construct(
        private readonly array $courses
    ) {
    }

    public function getCourses(): array
    {
        return $this->courses;
    }
}
