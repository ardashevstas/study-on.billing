<?php

namespace App\Response;

class SuccessResponse
{
    protected bool $success = true;

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}