<?php

namespace App\Response;

use App\Response\SuccessResponse;

class ProfileResponse extends SuccessResponse
{
    public function __construct(
        private readonly string $username,
        private readonly array $roles,
        private readonly float $balance
    ) {
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }
}