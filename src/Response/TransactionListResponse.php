<?php

namespace App\Response;

use App\Response\SuccessResponse;

class TransactionListResponse extends SuccessResponse
{
    public function __construct(
        private readonly array $transactions,
    ) {
    }

    public function getTransactions(): array
    {
        return $this->transactions;
    }
}