<?php

namespace App\Response;

use App\Response\SuccessResponse;

class RefreshTokenResponse extends SuccessResponse
{
    public function __construct(
        private readonly string $token
    ) {
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}