<?php

namespace App\Response;

use App\Response\SuccessResponse;

class BalanceResponse extends SuccessResponse
{
    public function __construct(
        private readonly string $current_balance
    ) {
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->current_balance;
    }
}