<?php

namespace App\Exception\Courses;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class CourseExistsException extends \Exception
{
    public function __construct($message, $code = Response::HTTP_CONFLICT, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}