<?php

namespace App\Exception\Courses;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class CourseCreationException extends \Exception
{
    public function __construct($message, $code = Response::HTTP_BAD_REQUEST, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}