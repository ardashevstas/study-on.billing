<?php

namespace App\Exception\User;

use RuntimeException;
use Throwable;

class UserNotFoundException extends RuntimeException
{
    public function __construct(string $message, int $code = 404, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}