<?php

namespace App\Exception\User;

use RuntimeException;
use Throwable;

class UserFieldsValidationException extends RuntimeException
{
    public function __construct(string $message, int $code = 400, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}