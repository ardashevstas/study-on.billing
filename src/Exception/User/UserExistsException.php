<?php

namespace App\Exception\User;

use RuntimeException;
use Throwable;

class UserExistsException extends RuntimeException
{
    public function __construct(string $message, int $code = 409, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
