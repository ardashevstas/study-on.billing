<?php

namespace App\Exception\Profile;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class TransactionCreationException extends \Exception
{
    public function __construct($message = 'Ошибка создания транзакции', $code = Response::HTTP_BAD_REQUEST, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
