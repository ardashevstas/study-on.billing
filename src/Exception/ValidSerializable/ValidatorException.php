<?php

namespace App\Exception\ValidSerializable;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ValidatorException extends Exception
{
    private readonly ConstraintViolationListInterface $validationResult;

    public function __construct(
        ConstraintViolationListInterface $validationResult,
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null
    ) {
        $this->validationResult = $validationResult;

        parent::__construct($message, $code, $previous);
    }

    public function getValidationResult(): ConstraintViolationListInterface
    {
        return $this->validationResult;
    }
}