<?php

namespace App\Exception\ValidSerializable;

use RuntimeException;
use Throwable;

class SerializerResolverException extends RuntimeException
{
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
