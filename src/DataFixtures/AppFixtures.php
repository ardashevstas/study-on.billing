<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\Transaction;
use App\Entity\User;
use App\Service\TransactionService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher,
        private float                       $startbalance,
        private TransactionService $transactionService
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadUserFixtures($manager);
        $this->loadCourseFixtures($manager);
    }

    private function loadUserFixtures(ObjectManager $manager): void
    {
        $usersData = [
            [
                'email' => 'ardashevstas@gmail.com',
                'role' => ['ROLE_SUPER_ADMIN'],
                'password' => 'ardashev'
            ],
            [
                'email' => 'ivanov@intaro.email',
                'role' => ['ROLE_USER'],
                'password' => 'ivanov'
            ]
        ];

        foreach ($usersData as $data) {
            $user = new User();
            $plaintextPassword = $data['password'];

            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );

            $user->setEmail($data['email']);
            $user->setRoles($data['role']);
            $user->setPassword($hashedPassword);
            $user->setBalance($this->startbalance);
            $manager->persist($user);

            $this->transactionService->createTransaction($user, null, 'deposit', $this->startbalance, false);
        }

        $manager->flush();
    }

    private function loadCourseFixtures(ObjectManager $manager): void
    {
        $courses = [
            ['code' => 'python', 'title' => 'Python', 'type' => 3, 'cost' => 25000],
            ['code' => 'vuejs', 'title' => 'Vue.js', 'type' => 1, 'cost' => 6000],
            ['code' => 'php', 'title' => 'PHP', 'type' => 1, 'cost' => 7000],
            ['code' => 'java', 'title' => 'Java', 'type' => 1, 'cost' => 8000],
            ['code' => 'go', 'title' => 'Go', 'type' => 3, 'cost' => 70000],
            ['code' => 'timemanagment', 'title' => 'Тайм-менеджмент', 'type' => 2, 'cost' => 0],
            ['code' => 'reactjs', 'title' => 'React.js', 'type' => 1, 'cost' => 7500],
            ['code' => 'nodejs', 'title' => 'Node.js', 'type' => 3, 'cost' => 30000],
            ['code' => 'typescript', 'title' => 'TypeScript', 'type' => 1, 'cost' => 6500],
            ['code' => 'fastapi', 'title' => 'FastApi', 'type' => 1, 'cost' => 16500],
            ['code' => 'sql', 'title' => 'SQL', 'type' => 1, 'cost' => 5500],
            ['code' => 'css3', 'title' => 'CSS3', 'type' => 2, 'cost' => 0]
        ];

        foreach ($courses as $courseData) {
            $course = new Course();
            $course->setCode($courseData['code']);
            $course->setType($courseData['type']);
            $course->setCost($courseData['cost']);
            $course->setTitle($courseData['title']);
            $manager->persist($course);
        }

        $manager->flush();
    }
}
