<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\CourseRepository;
use App\Response\BalanceResponse;
use App\Response\CoursePaymentResponse;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

class PaymentService
{
    public function __construct(
        private readonly Security $security,
        private readonly TransactionService $transactionService,
        private readonly UserService $userService,
        private readonly CourseRepository $courseRepository,
        private readonly CoursesService $coursesService
    ) {
    }

    public function payCourse(string $code)
    {
        $user = $this->security->getUser();
        if (null === $user) {
            throw new \Exception('Not authorized', JsonResponse::HTTP_UNAUTHORIZED);
        }

        $courseDb = $this->courseRepository->findByCode($code);
        if (!$courseDb) {
            throw new NotFoundHttpException('Course not found', null, Response::HTTP_NOT_FOUND);
        }

        $currentBalance = $user->getBalance();
        $cost = $courseDb->getCost();
        if ($cost > $currentBalance) {
            throw new \Exception('На вашем счету недостаточно средств', JsonResponse::HTTP_NOT_ACCEPTABLE);
        }

        $transaction = $this->transactionService->createTransaction(
            $user,
            $courseDb,
            'payment',
            $courseDb->getCost()
        );

        $newBalance = $currentBalance - $cost;
        $this->userService->updateUserBalance($user, $newBalance);

        if ($transaction->getExpirationDateTime() !== null) {
            $expirationDateTime = $transaction->getExpirationDateTime()->format('Y-m-d\TH:i:sP');
        }

        return new CoursePaymentResponse($this->coursesService->typeCourse[$courseDb->getType()], $expirationDateTime ?? '');
    }

    /**
     * @param float $amount
     * @return BalanceResponse|null
     * @throws Exception
     */
    public function topUp(float $amount): ?BalanceResponse
    {
        $user = $this->security->getUser();

        if (is_null($user)) {
            throw new AuthenticationException('User is not authenticated', Response::HTTP_UNAUTHORIZED);
        }

        $this->transactionService->createTransaction($user, null, 'deposit', $amount, false);

        $newBalance = $user->getBalance() + $amount;
        $this->userService->updateUserBalance($user, $newBalance);

        return new BalanceResponse($user->getBalance());
    }
}
