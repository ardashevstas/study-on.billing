<?php

namespace App\Service;

use App\Dto\ReviewRequestDTO;
use App\Entity\Review;
use App\Entity\User;
use App\Repository\ReviewRepository;
use App\Repository\CourseRepository;
use App\Response\ReviewResponse;
use App\Response\SuccessResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ReviewsService
{

    public function __construct(
        private readonly ReviewRepository $reviewRepository,
        private readonly CourseRepository $courseRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security
    ) {
    }

    /**
     * Получение списка отзывов для курса
     */
    public function getReviewsForCourse(string $courseCode): array
    {
        $course = $this->courseRepository->findByCode($courseCode);

        if (!$course) {
            throw new NotFoundHttpException('Курс не найден', null, 400);
        }

        $reviews = $this->reviewRepository->findBy(['course' => $course]);

        $reviewResponses = [];
        foreach ($reviews as $review) {
            $reviewResponses[] = new ReviewResponse(
                $review->getId(),
                $review->getUser()->getId(),
                $review->getUser()->getEmail(),
                $review->getCourse()->getId(),
                $review->getCourse()->getTitle(),
                $review->getRating(),
                $review->getComment(),
                $review->getCreatedAt()
            );
        }

        return $reviewResponses;
    }

    /**
     * Создание отзыва
     */
    public function createReview(ReviewRequestDTO $reviewRequestDTO, string $courseCode): SuccessResponse
    {
        $user = $this->security->getUser();

        if (!$user instanceof User) {
            throw new AccessDeniedException('User is not authenticated');
        }

        $course = $this->courseRepository->findByCode($courseCode);

        if (!$course) {
            throw new NotFoundHttpException('Course not found', null, 404);
        }

        $review = new Review();
        $review->setUser($user);
        $review->setCourse($course);
        $review->setRating($reviewRequestDTO->rating);
        $review->setComment($reviewRequestDTO->comment);

        $this->entityManager->persist($review);
        $this->entityManager->flush();

        return new SuccessResponse();
    }

    /**
     * Удаление отзыва
     */
    public function deleteReview(int $reviewId): SuccessResponse
    {
        $review = $this->reviewRepository->find($reviewId);

        if (!$review) {
            throw new NotFoundHttpException('Отзыв не найден', null, 404);
        }

        $user = $this->security->getUser();

        if (!in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            throw new AccessDeniedException('Access denied');
        }

        $this->entityManager->remove($review);
        $this->entityManager->flush();

        return new SuccessResponse();
    }
}
