<?php

namespace App\Service;

use App\Dto\RefreshTokenDTO;
use App\Dto\UserRegisterDTO;
use App\Exception\Profile\BalanceUpdateException;
use App\Exception\User\UserExistsException;
use App\Exception\User\UserNotFoundException;
use App\Repository\UserRepository;
use App\Response\ProfileResponse;
use App\Response\RefreshTokenResponse;
use App\Response\RegisterResponse;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Generator\RefreshTokenGeneratorInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly UserRepository $userRepository,
        private readonly JWTTokenManagerInterface $jwtManager,
        private readonly UserPasswordHasherInterface $passwordEncoder,
        private readonly RefreshTokenGeneratorInterface $refreshTokenGenerator,
        private readonly RefreshTokenManagerInterface $refreshTokenManager,
        private readonly Security $security,
    ) {
    }

    public function authorization(UserRegisterDTO $userRegisterDTO)
    {
        $user = $this->userRepository->findByEmail($userRegisterDTO->email);

        if (!$user) {
            throw new UserNotFoundException('No user found for email '. $userRegisterDTO->email);
        }

        $isValid = $this->passwordEncoder->isPasswordValid($user, $userRegisterDTO->password);

        if (!$isValid) {
            throw new BadRequestHttpException('Invalid password', null, 400);
        }

        $token = $this->jwtManager->create($user);

        $refreshToken = $this->refreshTokenGenerator->createForUserWithTtl(
            $user,
            (new \DateTime())->modify('+1 month')->getTimestamp()
        );

        $this->refreshTokenManager->save($refreshToken);

        return new RegisterResponse($token, $refreshToken->getRefreshToken());
    }

    public function registration(UserRegisterDTO $userRegisterDTO)
    {
        $existingUser = $this->userRepository->findByEmail($userRegisterDTO->email);

        if ($existingUser) {
            throw new UserExistsException('User already exists');
        }

        $user = User::fromDto($userRegisterDTO, $this->passwordEncoder);
        $user->setRoles(['ROLE_USER']);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $token = $this->jwtManager->create($user);

        // Создание refresh token
        $refreshToken = $this->refreshTokenGenerator->createForUserWithTtl(
            $user,
            (new \DateTime())->modify('+1 month')->getTimestamp()
        );
        // Сохранение refresh token в базе данных
        $this->refreshTokenManager->save($refreshToken);

        return new RegisterResponse($token, $refreshToken->getRefreshToken());
    }

    public function getProfile()
    {
        $user = $this->security->getUser();

        if (is_null($user)) {
            throw new AuthenticationException('User is not authenticated', Response::HTTP_UNAUTHORIZED);
        }

        return new ProfileResponse($user->getUserIdentifier(), $user->getRoles(), $user->getBalance());
    }

    public function refreshToken(RefreshTokenDTO $refreshTokenDTO)
    {
        $user = $this->userRepository->getUserByRefreshToken($refreshTokenDTO->refresh_token);

        if (!$user) {
            throw new BadRequestHttpException('Invalid refresh token', null, 400);
        }

        $newJwtToken = $this->jwtManager->create($user);

        return new RefreshTokenResponse($newJwtToken);
    }

    /**
     * @throws \Exception
     */
    public function updateUserBalance(UserInterface $user, float $newBalance)
    {
        try {
            $user->setBalance($newBalance);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            throw new BalanceUpdateException('Ошибка при обновлении баланса');
        }
    }
}
