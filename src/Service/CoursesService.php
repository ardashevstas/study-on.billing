<?php

namespace App\Service;

use App\Dto\CourseDTO;
use App\Dto\CourseRequestDTO;
use App\Entity\Course;
use App\Exception\Courses\CourseExistsException;
use App\Repository\CourseRepository;
use App\Response\CourseResponse;
use App\Response\CoursesResponse;
use App\Response\SuccessResponse;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

class CoursesService
{
    public array $typeCourse = [
        1 => 'rent',
        2 => 'free',
        3 => 'purchased'
    ];

    /**
     * @param CourseRepository $courseRepository
     * @param Security $security
     */
    public function __construct(
        private readonly CourseRepository $courseRepository,
        private readonly Security $security,
    ) {
    }

    /**
     * @return CoursesResponse
     */
    public function getCoursesList(): CoursesResponse
    {
        $coursesDb = $this->courseRepository->findAll();

        $courses = [];
        foreach ($coursesDb as $course) {
            $courses[] = new CourseDTO(
                $course->getCode(),
                $this->typeCourse[$course->getType()],
                max(0, $course->getCost())
            );
        }

        return new CoursesResponse($courses);
    }

    /**
     * @param string $code
     * @return CourseResponse
     */
    public function getCourse(string $code): CourseResponse
    {
        $courseDb = $this->courseRepository->findByCode($code);

        if (!$courseDb) {
            throw new NotFoundHttpException("Курс c кодом:{$code} не найден", null, Response::HTTP_NOT_FOUND);
        }

        return new CourseResponse($courseDb->getCode(), $this->typeCourse[$courseDb->getType()], $courseDb->getCost());
    }

    /**
     * @param CourseRequestDTO $courseRequestDTO
     * @return SuccessResponse
     * @throws CourseExistsException
     * @throws Exception
     */
    public function createCourse(CourseRequestDTO $courseRequestDTO): SuccessResponse
    {
        $this->checkUserRole();

        try {
            $course = new Course();
            $course->setTitle($courseRequestDTO->title)
                ->setCode($courseRequestDTO->code)
                ->setType($courseRequestDTO->type)
                ->setCost($courseRequestDTO->price);

            $this->courseRepository->save($course, true);
            return new SuccessResponse();
        } catch (UniqueConstraintViolationException $e) {
            throw new CourseExistsException('Курс с таким кодом существует');
        } catch (Exception $e) {
            throw new Exception("Ошибка добавления курса");
        }
    }

    /**
     * @param CourseRequestDTO $courseRequestDTO
     * @param string $code
     * @return SuccessResponse
     * @throws CourseExistsException
     * @throws Exception
     */
    public function editCourse(CourseRequestDTO $courseRequestDTO, string $code): SuccessResponse
    {
        $courseDb = $this->courseRepository->findByCode($code);
        if (!$courseDb) {
            throw new NotFoundHttpException('Course not found', null, Response::HTTP_UNAUTHORIZED);
        }

        $this->checkUserRole();

        try {
            $courseDb->setTitle($courseRequestDTO->title)
                ->setCode($courseRequestDTO->code)
                ->setType($courseRequestDTO->type)
                ->setCost($courseRequestDTO->price);

            $this->courseRepository->save($courseDb, true);
            return new SuccessResponse();
        } catch (UniqueConstraintViolationException $e) {
            throw new CourseExistsException('Курс с таким кодом существует');
        } catch (Exception $e) {
            throw new Exception("Ошибка обновления курса");
        }
    }

    /**
     * @throws Exception
     */
    public function deleteCourse(string $code): SuccessResponse
    {
        $courseDb = $this->courseRepository->findByCode($code);
        if (!$courseDb) {
            throw new NotFoundHttpException('Course not found', null, Response::HTTP_NOT_FOUND);
        }

        $this->checkUserRole();

        try {
            // Попытка удалить курс
            $this->courseRepository->remove($courseDb, true);
            return new SuccessResponse('Course deleted successfully');
        } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
            // Перехват исключения нарушения внешнего ключа
            throw new \Exception("Невозможно удалить курс, так как он уже был куплен пользователем", Response::HTTP_CONFLICT);
        } catch (Exception $e) {
            throw new Exception("Ошибка удаления курса");
        }
    }

    private function checkUserRole()
    {
        $user = $this->security->getUser();
        if (is_null($user)) {
            throw new AuthenticationException('User is not authenticated', Response::HTTP_UNAUTHORIZED);
        }

        // Проверяем, что пользователь авторизован и является администратором
        if (!in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            throw new AuthenticationException('Access denied', Response::HTTP_FORBIDDEN);
        }
    }
}
