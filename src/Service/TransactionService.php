<?php

namespace App\Service;

use App\Entity\Course;
use App\Entity\Transaction;
use App\Entity\User;
use App\Exception\Profile\TransactionCreationException;
use App\Response\TransactionListResponse;
use App\Response\TransactionResponse;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class TransactionService
{
    private $entityManager;
    private Security $security;
    private CoursesService $coursesService;
    private array $operationType = [
        'payment' => 1,
        'deposit' => 2
    ];

    public function __construct(EntityManagerInterface $entityManager, Security $security, CoursesService $coursesService)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->coursesService = $coursesService;
    }

    /**
     * @param User $user
     * @param Course|null $course
     * @param string $type
     * @param float $amount
     * @param bool $isRent
     * @return Transaction
     * @throws \Exception
     */
    public function createTransaction(UserInterface $user, ?Course $course, string $type, float $amount)
    {
        try {
            $transaction = new Transaction();

            $expirationDateTime = null;
            if (!is_null($course) && $this->coursesService->typeCourse[$course->getType()] === 'rent') {
                $expirationDateTime = new DateTime('+1 month');
            }

            $transaction
                ->setCourse($course)
                ->setUser($user)
                ->setOperationType($this->operationType[$type])
                ->setValue($amount)
                ->setTransactionDateTime(new DateTime())
                ->setExpirationDateTime($expirationDateTime)
            ;

            $this->entityManager->persist($transaction);
            $this->entityManager->flush();

            return $transaction;
        } catch (\Exception $e) {
            throw new TransactionCreationException('Ошибка создания транзакции');
        }
    }

    public function getListTransaction(Request $request)
    {
        $user = $this->security->getUser();
        if (is_null($user)) {
            throw new AuthenticationException('User is not authenticated', Response::HTTP_UNAUTHORIZED);
        }

        $filter = $request->query->get('filter');
        if (isset($filter['type'])) {
            $filter['type'] = $this->operationType[$filter['type']];
        }

        $transactions = $this->entityManager->getRepository(Transaction::class)->findByUserAndFilters($user, $filter);
        $type = array_flip($this->operationType);

        $response = [];
        foreach ($transactions as $item) {
            $response[] = new TransactionResponse(
                $item->getId(),
                $item->getTransactionDateTime()->format('Y-m-d\TH:i:sP'),
                $item->getExpirationDateTime() !== null ? $item->getExpirationDateTime()->format('Y-m-d\TH:i:sP') : '',
                $type[$item->getOperationType()],
                $item->getValue(),
                $item->getCourse() !== null ? $item->getCourse()->getCode() : null
            );
        }

        return new TransactionListResponse($response);
    }
}
