<?php

namespace App\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER)]
class Serializable
{
    /**
     * @param null|array<string> $groups
     */
    public function __construct(private readonly ?array $groups = null)
    {
    }

    /**
     * @return null|string[]
     */
    public function getGroups(): ?array
    {
        return $this->groups;
    }
}