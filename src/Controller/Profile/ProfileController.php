<?php

namespace App\Controller\Profile;

use App\Attribute\ValidSerializable;
use App\Dto\TopupDTO;
use App\Response\BalanceResponse;
use App\Response\BaseErrorResponse;
use App\Response\CoursePaymentResponse;
use App\Response\ProfileResponse;
use App\Response\TransactionResponse;
use App\Service\PaymentService;
use App\Service\TransactionService;
use App\Service\UserService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Throwable;

class ProfileController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
        private readonly PaymentService $paymentService,
        private readonly TransactionService $transactionService,
    ) {
    }

    /**
     * Получение текущего пользователя
     */
    #[Route('/api/v1/users/current', name: 'user_profile', methods: ['GET'])]
    #[OA\Tag(name: 'profile')]
    #[OA\Response(
        response: 200,
        description: 'Current user data',
        content: new Model(type: ProfileResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Error response',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function getCurrentUser(): Response
    {
        try {
            return $this->json($this->userService->getProfile());
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Пополнение баланса
     */
    #[Route('/api/v1/top-up', name: 'user_topup', methods: ['POST'])]
    #[OA\Tag(name: 'profile')]
    #[OA\RequestBody(
        content: new Model(type: TopupDTO::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new Model(type: BalanceResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Ошибка создания транзакции',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function topUpBalance(
        #[ValidSerializable] TopupDTO $topupDTO,
    ): Response
    {
        try {
            return $this->json($this->paymentService->topUp($topupDTO->amount));
        } catch (Throwable $exception) {
            return $this->json(
                new BaseErrorResponse($exception->getMessage()),
                $exception->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Получение истории транзакций
     */
    #[Route('/api/v1/transactions', name: 'user_transactions', methods: ['GET'])]
    #[OA\Tag(name: 'profile')]
    #[OA\Parameter(
        name: 'filter[type]',
        description: 'Тип транзакции payment|deposit',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'filter[course_code]',
        description: 'Символьный код курса',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'filter[skip_expired]',
        description: 'Флаг, позволяющий отбросить записи с датой expires_at в прошлом',
        in: 'path',
        schema: new OA\Schema(type: 'boolean')
    )]
    #[OA\Response(
        response: 200,
        description: 'Current user data',
        content: new Model(type: TransactionResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Error response',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function getTransactionsHistory(
        Request $request
    ): Response
    {
        try {
            return $this->json($this->transactionService->getListTransaction($request));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Оплата курса
     */
    #[Route('/api/v1/courses/{code}/pay', name: 'pay_course', methods: ['GET'])]
    #[OA\Tag(name: 'profile')]
    #[OA\Parameter(
        name: 'code',
        description: 'Символьный код курса',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new Model(type: CoursePaymentResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 404,
        description: 'Course not found',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 406,
        description: 'Not enough funds in your account',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function payCourse(string $code): Response
    {
        try {
            return $this->json($this->paymentService->payCourse($code));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }
}