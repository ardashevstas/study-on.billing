<?php

namespace App\Controller\Profile;

use App\Attribute\ValidSerializable;
use App\Dto\ReviewRequestDTO;
use App\Response\BaseErrorResponse;
use App\Response\ReviewResponse;
use App\Response\SuccessResponse;
use App\Service\ReviewsService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Throwable;
use Symfony\Component\Security\Core\Security as UserSecurity;

class ReviewsController extends AbstractController
{
    public function __construct(
        private readonly ReviewsService $reviewsService,
        private readonly UserSecurity $security,
    ) {
    }

    /**
     * Получение списка отзывов для курса
     */
    #[Route('/api/v1/courses/{courseCode}/reviews', name: 'reviews', methods: ['GET'])]
    #[OA\Tag(name: 'reviews')]
    #[OA\Parameter(
        name: 'courseCode',
        description: 'Код курса',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Response(
        response: 200,
        description: 'List of reviews',
        content: new Model(type: ReviewResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Error response',
        content: new Model(type: BaseErrorResponse::class)
    )]
    public function getReviewsForCourse(string $courseCode): Response
    {
        try {
            return $this->json($this->reviewsService->getReviewsForCourse($courseCode));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Добавление отзыва
     */
    #[Route('/api/v1/courses/{courseCode}/reviews', name: 'add_review', methods: ['POST'])]
    #[OA\Tag(name: 'reviews')]
    #[OA\Parameter(
        name: 'courseCode',
        description: 'Код курса',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\RequestBody(
        content: new Model(type: ReviewRequestDTO::class)
    )]
    #[OA\Response(
        response: 201,
        description: 'Review created',
        content: new Model(type: SuccessResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Ошибка добавления отзыва',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function addReview(
        #[ValidSerializable] ReviewRequestDTO $reviewRequestDTO,
        string $courseCode
    ): Response
    {
        try {
            return $this->json($this->reviewsService->createReview($reviewRequestDTO, $courseCode));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Удаление отзыва
     */
    #[Route('/api/v1/reviews/{id}', name: 'delete_review', methods: ['DELETE'])]
    #[OA\Tag(name: 'reviews')]
    #[OA\Parameter(
        name: 'id',
        description: 'ID курса',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Response(
        response: 200,
        description: 'Review deleted',
        content: new Model(type: SuccessResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Ошибка удаления отзыва',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 403,
        description: 'Access denied',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function deleteReview(int $id): Response
    {
        $user = $this->security->getUser();

        // Проверяем роль пользователя
        if (!in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            return $this->json(new BaseErrorResponse('Access denied'), Response::HTTP_FORBIDDEN);
        }

        try {
            return $this->json($this->reviewsService->deleteReview($id));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }
}
