<?php

namespace App\Controller\Courses;

use App\Attribute\ValidSerializable;
use App\Dto\CourseDTO;
use App\Dto\CourseRequestDTO;
use App\Response\BaseErrorResponse;
use App\Response\CoursesResponse;
use App\Response\SuccessResponse;
use App\Service\CoursesService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Throwable;

class CoursesController extends AbstractController
{
    public function __construct(
        private readonly CoursesService $coursesService,
    ) {
    }

    /**
     * Получение списка курсов
     */
    #[Route('/api/v1/courses', name: 'courses', methods: ['GET'])]
    #[OA\Tag(name: 'courses')]
    #[OA\Response(
        response: 200,
        description: 'List of courses',
        content: new Model(type: CoursesResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Error response',
        content: new Model(type: BaseErrorResponse::class)
    )]
    public function getCourses(): Response
    {
        try {
            return $this->json($this->coursesService->getCoursesList());
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Получение кода по курсу
     */
    #[Route('/api/v1/courses/{code}', name: 'course', methods: ['GET'])]
    #[OA\Tag(name: 'courses')]
    #[OA\Parameter(
        name: 'code',
        description: 'Символьный код курса',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new Model(type: CourseDTO::class)
    )]
    #[OA\Response(
        response: 404,
        description: 'Course not found',
        content: new Model(type: BaseErrorResponse::class)
    )]
    public function getCourseByCode(string $code): Response
    {
        try {
            return $this->json($this->coursesService->getCourse($code));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Добавление курса
     */
    #[Route('/api/v1/courses', name: 'add_course', methods: ['POST'])]
    #[OA\Tag(name: 'courses')]
    #[OA\RequestBody(
        content: new Model(type: CourseRequestDTO::class)
    )]
    #[OA\Response(
        response: 201,
        description: 'CourseExistsException created',
        content: new Model(type: SuccessResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Ошибка добавления курса',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 404,
        description: 'Course not found',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 409,
        description: 'Курс с таким кодом существует',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function addCourse(
        #[ValidSerializable] CourseRequestDTO $courseRequestDTO
    ): Response
    {
        try {
            return $this->json($this->coursesService->createCourse($courseRequestDTO));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Редактирование курса
     */
    #[Route('/api/v1/courses/{code}', name: 'edit_course', methods: ['POST'])]
    #[OA\Tag(name: 'courses')]
    #[OA\RequestBody(
        content: new Model(type: CourseRequestDTO::class)
    )]
    #[OA\Response(
        response: 201,
        description: 'Course updated',
        content: new Model(type: SuccessResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Ошибка обновления курса',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 404,
        description: 'Course not found',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 409,
        description: 'Курс с таким кодом существует',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function editCourse(
        #[ValidSerializable] CourseRequestDTO $courseRequestDTO,
        string $code
    ): Response
    {
        try {
            return $this->json($this->coursesService->editCourse($courseRequestDTO, $code));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Удаление курса
     */
    #[Route('/api/v1/courses/{code}', name: 'delete_course', methods: ['DELETE'])]
    #[OA\Tag(name: 'courses')]
    #[OA\Response(
        response: 200,
        description: 'Course deleted',
        content: new Model(type: SuccessResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Ошибка удаления курса',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 401,
        description: 'User is not authenticated',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 404,
        description: 'Course not found',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[Security(name: 'Bearer')]
    public function deleteCourse(
        string $code
    ): Response
    {
        try {
            return $this->json($this->coursesService->deleteCourse($code));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }
}