<?php

namespace App\Controller\User;

use App\Attribute\ValidSerializable;
use App\Dto\RefreshTokenDTO;
use App\Dto\UserRegisterDTO;
use App\Response\BaseErrorResponse;
use App\Response\RefreshTokenResponse;
use App\Response\RegisterResponse;
use App\Service\UserService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Throwable;

class AuthController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    /**
     * Авторизация и выдача jwt-токена
     */
    #[Route('/api/v1/auth', name: 'user_auth', methods: ['POST'])]
    #[OA\Tag(name: 'user')]
    #[OA\RequestBody(
        content: new Model(type: UserRegisterDTO::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Successful authentication',
        content: new Model(type: RegisterResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Error response',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 404,
        description: 'User not found',
        content: new Model(type: BaseErrorResponse::class)
    )]
    public function auth(
        #[ValidSerializable] UserRegisterDTO $userRegisterDTO
    ): Response
    {
        try {
            return $this->json($this->userService->authorization($userRegisterDTO));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }

    /**
     * Обновление токена
     */
    #[Route('/api/v1/token/refresh', name: 'user_refresh_token', methods: ['POST'])]
    #[OA\Tag(name: 'user')]
    #[OA\RequestBody(
        content: new Model(type: RefreshTokenDTO::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'New JWT token',
        content: new Model(type: RefreshTokenResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Invalid refresh token',
        content: new Model(type: BaseErrorResponse::class)
    )]
    public function refreshToken(
        #[ValidSerializable] RefreshTokenDTO $refreshTokenDTO
    ): Response
    {
        try {
            return $this->json($this->userService->refreshToken($refreshTokenDTO));
        } catch (Throwable $exception) {
            return $this->json(new BaseErrorResponse($exception->getMessage()), $exception->getCode());
        }
    }
}