<?php

namespace App\Controller\User;

use App\Attribute\ValidSerializable;
use App\Dto\UserRegisterDTO;
use App\Exception\User\UserExistsException;
use App\Response\BaseErrorResponse;
use App\Response\RegisterResponse;
use App\Service\UserService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Throwable;

class RegistrationController extends AbstractController
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    /**
     * Регистрация
     */
    #[Route('/api/v1/register', name: 'user_register', methods: ['POST'])]
    #[OA\Tag(name: 'user')]
    #[OA\RequestBody(
        content: new Model(type: UserRegisterDTO::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'User registered successfully',
        content: new Model(type: RegisterResponse::class)
    )]
    #[OA\Response(
        response: 400,
        description: 'Error response',
        content: new Model(type: BaseErrorResponse::class)
    )]
    #[OA\Response(
        response: 409,
        description: 'User already exists',
        content: new Model(type: BaseErrorResponse::class)
    )]
    public function register(
        #[ValidSerializable] UserRegisterDTO $userRegisterDTO,
        Request $request
    ): Response {
        try {
            return $this->json($this->userService->registration($userRegisterDTO));

        } catch (Throwable $exception) {
            return $this->json(
                new BaseErrorResponse($exception->getMessage()),
                $this->getErrorCodeFromException($exception)
            );
        }
    }

    protected function getErrorCodeFromException(Throwable $exception): int
    {
        return match (get_class($exception)) {
            UserExistsException::class => Response::HTTP_CONFLICT,
            default => Response::HTTP_INTERNAL_SERVER_ERROR,
        };
    }
}
