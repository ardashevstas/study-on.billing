<?php

namespace App\Tests;

use App\Controller\ApiController;
use App\DataFixtures\AppFixtures;
use App\Entity\Course;
use App\Repository\UserRepository;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ApiControllerTest extends AbstractTest
{
    private string $correctEmail = 'ardashevstas@gmail.com';
    private string $password = 'ardashev';
    private static function auth($client, $email, $password)
    {
        $client->request('POST', '/api/v1/auth', [], [], [], json_encode([
            'email' => $email,
            'password' => $password
        ]));

        return $client->getResponse();
    }

    /**
     * Load fixtures before test
     */
    protected function getFixtures(): array
    {
        return [
            AppFixtures::class
        ];
    }

    /*
     * Тест авторизации.
     */
    public function testAuth(): void
    {
        $client = static::getClient();

        // 1 - тест авторизации с неверным email
        $wrongEmail = 'wrong@gmail.com';
        $wrongPassword = 'wrongpassword';

        $response = self::auth($client, $wrongEmail, $wrongPassword);

        $this->assertResponseCode(400, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('No user found for email ' . $wrongEmail, $responseContent['error']['message']);

        // 2 - Тест авторизации с неверным паролем
        $response = self::auth($client, $this->correctEmail, $wrongPassword);

        $this->assertResponseCode(404, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('Invalid password', $responseContent['error']['message']);

        // Тест успешной авторизации
        $response = self::auth($client, $this->correctEmail, $this->password);

        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);
        $this->assertArrayHasKey('refresh_token', $responseContent);
    }

    /*
     * Тест регистрации.
     */
    public function testRegister(): void
    {
        $client = static::getClient();

        // Тест регистрации с существующим email
        $client->request('POST', '/api/v1/register', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'email' => 'ardashevstas@gmail.com',
            'password' => 'ardashev',
        ], JSON_THROW_ON_ERROR));

        $this->assertResponseCode(409, $client->getResponse());
        $responseContent = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('User already exists', $responseContent['error']['message']);

        // Тест успешной регистрации
        $client->request('POST', '/api/v1/register', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'email' => 'newuser@example.com',
            'password' => 'password',
        ]));

        $this->assertResponseCode(201, $client->getResponse());
        $responseContent = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);
        $this->assertArrayHasKey('refresh_token', $responseContent);
    }

    /*
     * Тест получения текущего пользователя
     */
    public function testGetCurrentUser(): void
    {
        $client = static::getClient();

        // авторизуемся, получаем токен
        $response = self::auth($client, $this->correctEmail, $this->password);
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);
        $this->assertArrayHasKey('refresh_token', $responseContent);

        // пытеамся получить пользователя по токену
        $token = $responseContent['token'];
        $client->request('GET', '/api/v1/users/current', [], [], ['HTTP_AUTHORIZATION' => 'Bearer '. $token]);

        $response = $client->getResponse();
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('username', $responseContent);
        $this->assertArrayHasKey('roles', $responseContent);
        $this->assertArrayHasKey('balance', $responseContent);

        $this->assertEquals('ardashevstas@gmail.com', $responseContent['username']);
        $this->assertEquals(['ROLE_SUPER_ADMIN', 'ROLE_USER'], $responseContent['roles']);
        $this->assertEquals(50000, $responseContent['balance']);
    }

    /**
     * Тест обновления токена
     */
    public function testRefreshToken(): void
    {
        $client = static::getClient();

        // получение токена и токена обновления
        $response = self::auth($client, $this->correctEmail, $this->password);

        // проверяем успешную авторизацию
        $this->assertResponseCode(200, $response);
        $refreshToken = json_decode($response->getContent(), true)['refresh_token'];

        // отправляем запрос с некорретным токеном
        $client->request('POST', '/api/v1/token/refresh', [], [], [], json_encode([
            'refresh_token' => 'it\'s a wrong refresh token'
        ]));

        $response = $client->getResponse();
        $this->assertResponseCode(400, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('error', $responseContent);

        // отправляем запрос с пустым параметром
        $client->request('POST', '/api/v1/token/refresh', [], [], [], json_encode([
            'refresh_token' => ''
        ]));

        $response = $client->getResponse();
        $this->assertResponseCode(400, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('error', $responseContent);

        // проверяем обновление токена
        $client->request('POST', '/api/v1/token/refresh', [], [], [], json_encode([
            'refresh_token' => $refreshToken
        ]));

        $this->assertResponseCode(200, $client->getResponse());
        $responseContent = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);
    }

    /**
     * Тест получения курсов
     */
    public function testGetCourses(): void
    {
        $client = static::getClient();

        // проверяем что курсы достаются из БД
        $client->request('GET', '/api/v1/courses', [], [], []);

        $response = $client->getResponse();
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('courses', $responseContent);

        $this->assertGreaterThan(0, count($responseContent['courses']));

        // теперь неоходимо очистить таблицу курсов и проверить на ошибку 400 "Courses not found"
        // получить entity manager
        $entityManager = static::$container->get('doctrine.orm.entity_manager');

        // получить все курсы
        $courses = $entityManager->getRepository(Course::class)->findAll();

        // удалить все курсы
        foreach ($courses as $course) {
            $entityManager->remove($course);
        }
        $entityManager->flush();

        $client->request('GET', '/api/v1/courses', [], [], []);

        $response = $client->getResponse();
        $this->assertResponseCode(404, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('Courses not found', $responseContent['error']['message']);
    }

    /**
     * Тест получения курса
     */
    public function testGetCourse(): void
    {
        $client = static::getClient();

        // запрос с несуществующим курсом
        $client->request('GET', '/api/v1/courses/wrong');

        $response = $client->getResponse();
        $this->assertResponseCode(404, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('CourseExistsException not found', $responseContent['error']['message']);


        $client->request('GET', '/api/v1/courses/php');

        $response = $client->getResponse();
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('course', $responseContent);
        $this->assertEquals('php', $responseContent['course']['code']);
    }

    /**
     * Тест оплвты курсов
     */
    public function testPayCourse(): void
    {
        $client = static::getClient();
        // 1 - авторизуемся
        $authResponse = self::auth($client, $this->correctEmail, $this->password);

        $this->assertResponseCode(200, $authResponse);
        $responseContent = json_decode($authResponse->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);

        $token = $responseContent['token'];

        // 2 - проверка 404 'CourseExistsException not found'
        $client->request('POST', '/api/v1/courses/wrong/pay', [], [], ['HTTP_AUTHORIZATION' => 'Bearer '. $token]);

        $response = $client->getResponse();
        $this->assertResponseCode(404, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('CourseExistsException not found', $responseContent['error']['message']);

        // 3 - проверяем оплату курса
        $client->request('POST', '/api/v1/courses/php/pay', [], [], ['HTTP_AUTHORIZATION' => 'Bearer '. $token]);

        $response = $client->getResponse();
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('true', $responseContent['success']);
    }

    /**
     * Тест пополнения баланса
     */
    public function testTopUp(): void
    {
        $client = static::getClient();
        // 1 - авторизуемся
        $authResponse = self::auth($client, $this->correctEmail, $this->password);

        $this->assertResponseCode(200, $authResponse);
        $responseContent = json_decode($authResponse->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);

        $token = $responseContent['token'];

        // 2 - тест с некорректным значением
        $client->request('POST', '/api/v1/top-up', [], [], ['HTTP_AUTHORIZATION' => 'Bearer '. $token], json_encode([
            'amount' => '0'
        ]));

        $response = $client->getResponse();
        $this->assertResponseCode(400, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('error', $responseContent);
        $this->assertEquals('Некорректная сумма', $responseContent['error']['message']);

        // 3 - тест с корректным значением
        $client->request('POST', '/api/v1/top-up', [], [], ['HTTP_AUTHORIZATION' => 'Bearer '. $token], json_encode([
            'amount' => '50000'
        ]));

        $response = $client->getResponse();
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertEquals('true', $responseContent['success']);
    }

    /**
     * Тест получения истории транзакций
     */
    public function testTransactionHistory(): void
    {
        $client = static::getClient();
        // 1 - авторизуемся
        $authResponse = self::auth($client, $this->correctEmail, $this->password);

        $this->assertResponseCode(200, $authResponse);
        $responseContent = json_decode($authResponse->getContent(), true);
        $this->assertArrayHasKey('token', $responseContent);

        $token = $responseContent['token'];

        $client->request('GET', '/api/v1/transactions', [], [], ['HTTP_AUTHORIZATION' => 'Bearer '. $token]);

        $response = $client->getResponse();
        $this->assertResponseCode(200, $response);
        $responseContent = json_decode($response->getContent(), true);
        $this->assertGreaterThan(0, count($responseContent['data']));
    }
}
