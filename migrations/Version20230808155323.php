<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230808155323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course ADD title VARCHAR(255) NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_169E6FB92B36786B ON course (title)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_169E6FB92B36786B');
        $this->addSql('ALTER TABLE course DROP title');
    }
}
