# StudyOnBilling

Система управления онлайн-курсами — это веб-приложение, которое позволяет администраторам создавать и редактировать курсы, а пользователям — просматривать, оплачивать и проходить курсы. 
Пользователи могут оставлять отзывы на курсы, оценивая их качество и предоставляя комментарии. 
Также пользователи могут регистрироваться в системе, пополнять свой баланс и использовать средства для оплаты выбранных курсов. 
Система ведёт учёт транзакций и предоставляет историю операций для каждого пользователя.


## Требования

- **Docker** и **Docker Compose** установлены на вашей машине.
- **Git** для клонирования репозитория.

## Установка и запуск

### 1. Клонирование репозитория

Склонируйте проект на локальную машину с использованием следующей команды:

```bash
git clone https://gitlab.com/ardashevstas/study-on.billing.git
```

Перейдите в директорию проекта:

```bash
cd study-on.billing
```

### 2. Создание файла окружения

Создайте файл .env на основе .env.example, чтобы задать параметры подключения к базе данных и другие настройки:

```bash
cp .env.example .env
```

Отредактируйте файл .env, если необходимо, например, чтобы задать параметры подключения к базе данных.

### 3. Запуск контейнеров Docker

Используйте команду docker-compose для запуска всех необходимых контейнеров (PostgreSQL, PHP и др.):

```bash
docker-compose up -d
```

Эта команда запустит контейнеры в фоновом режиме. Убедитесь, что контейнеры запущены успешно:

```bash
docker-compose ps
```

### 4. Установка зависимостей

В контейнере приложения необходимо установить все PHP-зависимости с помощью Composer. Для этого выполните команду:

```bash
make install
```

Эта команда запустит установку всех необходимых библиотек и зависимостей, которые определены в файле composer.json.

### 5. Создание БД

```bash
make create
```

### 6. Миграция базы данных

```bash
make migrate
```

### 7. Инициализация данных

Если нужно, выполните инициализацию данных, например, создание тестовых записей в базе данных. Для этого выполните:

```bash
make fixtload
```

Эта команда заполнит базу данных начальными данными, которые могут быть полезны для тестирования и разработки.

### 8. Доступ к приложению

После запуска контейнеров ваше приложение будет доступно по адресу:

http://localhost:82/api/v1/doc


Чтобы обращаться к проекту по домену, необходимо зарегистрировать его в локальном hosts-файле:

```bash
sudo cat /etc/hosts

127.0.0.1 study-on.local
```